import java.io.*;
        import java.math.*;
        import java.security.*;
        import java.text.*;
        import java.util.*;
        import java.util.concurrent.*;
        import java.util.regex.*;

public class Main {

    // Complete the hourglassSum function below.
    static int hourglassSum(int[][] arr) {
        int maxSum=-64;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                int tmp = arr[i + 1][j + 1];
                for (int k = 0; k < 3; k++) {
                    tmp += arr[i][j + k] + arr[i + 2][j + k];
                }
                if (tmp >= maxSum)
                    maxSum = tmp;
            }

        }

        return maxSum;
    }


    public static void main(String[] args) throws IOException {

        int[][] arr = {
                {1,1,1,0,0,0},
                {0,1,0,0,0,0},
                {1,1,1,0,0,0},
                {0,0,2,4,4,0},
                {0,0,0,2,0,0},
                {0,0,1,2,4,0}
        };
        int result = hourglassSum(arr);
        System.out.println(result);

    }
}
